# IBM MQTT Client  

This project shows the usage of the Eclipse [Paho MQTT](https://www.eclipse.org/paho/clients/java/) java library and the [IBM Bluemix IoT Platform](https://console.bluemix.net/catalog/services/internet-of-things-platform) specific settings to connect and publish messages.  

## Files  
**libs/org.eclipse.paho.client.mqttv3-1.2.0.jar** :  
The Eclipse Paho MQTT library  

**MyMQTTClient.java** :  
This file uses the Paho MQTT client library to show a standard way to connect and publish to an MQTT broker.  

**Main.java** :  
This file uses the MyMQTTClient class contains the IBM specific MQTT setting to connect to an IBM IoT Platform.  

## Running
First you need to create an IoT Platform in the IBM Bluemix cloud and register a device. Jump to the **How to set up an IBM IoT Platform** part for a quick guide.

### CLI  
Clone the repo:  
```
git clone https://gitlab.com/tamaspflanzner/ibmmqqtclient.git
```

Modify the variables in the Main java file:  
**See the How to set up an IBM IoT Platform part**  
`IBM_ORGANIZATION_ID`:  
The IBM IoT Platform id  
`IBM_DEVICE_TYPE`:  
The type of the registered device in your IBM IoT Platform  
`IBM_DEVICE_ID`:  
The name of the registered device in your IBM IoT Platform  
`IBM_DEVICE_TOKEN`:  
The password of the registered device in your IBM IoT Platform  

Enter to the downloaded folder:
```
cd ibmmqttclient
```

Compile:
```
javac -cp ".:./libs/org.eclipse.paho.client.mqttv3-1.2.0.jar" ./src/MyMQTTClient.java ./src/Main.java 
```
Run:
```
cd src
java -cp ".:../libs/org.eclipse.paho.client.mqttv3-1.2.0.jar" Main
```

Example output:

>Connecting to ssl://zt3tev.messaging.internetofthings.ibmcloud.com:443  
clientID: d:zt3tev:testtype:testdevice  
username: use-token-auth password: 88888888  
Connected!  
Publishing to topic: iot-2/evt/status/fmt/json  
message: {"d":{"value":57}}  
Published!  
Publishing to topic: iot-2/evt/status/fmt/json  
message: {"d":{"value":89}}  
Published!  
Publishing to topic: iot-2/evt/status/fmt/json  
message: {"d":{"value":38}}  
Published!  
Publishing to topic: iot-2/evt/status/fmt/json  
message: {"d":{"value":61}}  
Published!  
Publishing to topic: iot-2/evt/status/fmt/json  
message: {"d":{"value":46}}  
Published!  
Publishing to topic: iot-2/evt/status/fmt/json  
message: {"d":{"value":19}}  
Published!  
Publishing to topic: iot-2/evt/status/fmt/json  
message: {"d":{"value":45}}  
Published!  
Publishing to topic: iot-2/evt/status/fmt/json  
message: {"d":{"value":18}}  
Published!  
Publishing to topic: iot-2/evt/status/fmt/json  
message: {"d":{"value":22}}  
Published!  
Publishing to topic: iot-2/evt/status/fmt/json  
message: {"d":{"value":65}}  
Published!  
Disconnecting...  
Disconnected!  


### IntelliJ
Download the project in zip format and extract it, or clone the repo

>File -> Open...
Select the ibmmqttclient folder
Press OK

Don't worry, if the MyMQTTClient java file has errors, add the Paho jar to the project:
In the project view 
>right click on the libs folder -> Add as Library... -> OK

Modify the variables in the Main java file:  
**See the How to set up an IBM IoT Platform part**  
`IBM_ORGANIZATION_ID`:  
The IBM IoT Platform id  
`IBM_DEVICE_TYPE`:  
The type of the registered device in your IBM IoT Platform  
`IBM_DEVICE_ID`:  
The name of the registered device in your IBM IoT Platform  
`IBM_DEVICE_TOKEN`:  
The password of the registered device in your IBM IoT Platform  

Now you can run the Main java file:  
>Run -> Run... -> Main


## How to set up an IBM IoT Platform
Create an account at https://console.bluemix.net/ and log in.  
  
Create a Resource.  
![Create a Resource screenshot](screenshots/create_resource.png)  

Select the Internet of Things Platform and create it.  
![Internet of Things Platform screenshot](screenshots/create_iot_platform.png)  

Click on the created service at the Dashboard  
![Service at the Dashboard screenshot](screenshots/select_iot_platform.png)  

Launch it to open the Dashboard of the service  
![Launch screenshot](screenshots/launch.png)  

Create a device type by selecting the Device Types and clicking on the + Add Device Type button (`testtype` in the example)  
![Create a device type screenshot](screenshots/create_device_type.png)  

Go back to Browse the devices and click on the +Add Device button  
![Add a device screenshot](screenshots/add_device.png)  

Select the previously created Device Type and edit the Device ID (this is the name of the device) (`testdevice` in the example)  
![Device ID screenshot](screenshots/device_id.png)  

After clicking the Next button, edit the Authentication Token (if you forget the authentication token, it can not be recovered) (`88888888` in the example)  
![Authentication Token screenshot](screenshots/token.png)  

On the last page you can see the credentials of the new device. Take a note on the organization id (`zt3tev` in the example)  
![Credentials screenshot](screenshots/credentials.png)  

Go back to Browse Devices, select the new device and click on the Recent Events. If you modify the variables (IBM_ORGANIZATION_ID, IBM_DEVICE_TYPE, IBM_DEVICE_ID, IBM_DEVICE_TOKEN) in the Main.java file and run the application, you will see the published messages in real time.  
![Recent Events screenshot](screenshots/recent_events.png)  
